import argparse
import logging
from os import getenv
import re
import sys
import time
from urllib.parse import urljoin


from bs4 import BeautifulSoup
import requests
from requests.exceptions import RequestException


if getenv('info'):
    logging.basicConfig(level=logging.INFO, format='%(message)s',)


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-u', '--url',
        type=check_url,
        required=True,
        help='root url to start from'
    )
    parser.add_argument(
        '-d', '--depth',
        type=int,
        default=1,
        help='max depth to crawl from root url'
    )
    return parser


def check_url(url):
    response = requests.head(url)
    response.raise_for_status()
    return url


def obtain_args():
    parser = create_parser()
    namespace = parser.parse_args(sys.argv[1:])
    return namespace


def fetch_page(
        url,
        headers={
            'User-Agent': (
                'Mozilla/5.0 (X11; Linux x86_64) '
                'AppleWebKit/537.36 (KHTML, like Gecko) '
                'Chrome/62.0.3202.94 Safari/537.36'
            )
        },
        timeout=5,
        retries=3,
    ):
    while True:
        try:
            rps = requests.get(url, headers=headers)
            logging.info('status code : {code}'.format(code=rps.status_code))
            if not rps.ok:
                return None
            return rps.text
        except RequestException as error:
            logging.info(
                'During fetch_page error has happend: {err_mess}'
                .format(err_mess=error.message)
            )
            if not retries:
                return None
            retries -= 1
            time.sleep(timeout)
            timeout += timeout


def fetch_emails_with_crawler(url_root, crawling_depth, timeout=1, first=0):
    root_page = fetch_page(url_root)
    emails = extract_unique_emails(root_page)
    if crawling_depth <= 0:
        return emails
    root_urls = extract_unique_urls(root_page, url_root)
    all_urls = root_urls
    parent_urls = root_urls
    for stage in range(first, crawling_depth):
        children_urls = set()
        logging.info('\nStarting {number} urls at {stage} stage\n'
            .format(number=len(parent_urls), stage=stage)
        )
        for url in parent_urls:
            logging.info('fetching {url}'.format(url=url))
            page = fetch_page(url)
            if not page:
                continue
            emails.update(extract_unique_emails(page))
            children_urls.update(extract_unique_urls(page, url))
            logging.info(
                'Fetched {number} emails\n'.format(number=len(emails))
            )
            time.sleep(timeout)
        # to stop on the last iteration further code execution
        if stage == crawling_depth - 1:
            break
        # removes urls found in all_urls to prevent from recurrence
        children_urls.difference_update(all_urls)
        all_urls.update(children_urls)
        parent_urls = children_urls
    return emails


def extract_unique_emails(from_html):
    mail_pattern = re.compile(
        r'[\w\-][\w\-\.]+@[\w\-][\w\-\.]+[a-zA-Z]{1,4}'
    )
    return {email for email in mail_pattern.findall(from_html)}


def extract_unique_urls(from_html, url):
    soup = BeautifulSoup(from_html, 'html.parser')
    return {
        urljoin(url, tag['href']) for tag in soup.find_all('a', href=True)
    }


def output_emails_to_console(emails, url, depth):
    print(
        '\nTotal {number} emails from {url} with crawling_depth = {depth}\n'
        .format(number=len(emails), url=url, depth=depth)
    )
    for number, email in enumerate(emails, start=1):
        print('{number:<3}| {email}'.format(number=number, email=email))
    print()


if __name__ == '__main__':
    args = obtain_args()
    output_emails_to_console(
        fetch_emails_with_crawler(args.url, args.depth),
        args.url,
        args.depth,
    )
