# Simple email crawler #

This script can extract emails from sites, like forums.

##Features:

- filters cyclic links
- settable crawling depth
- prints unique email addresses to console
- can log process to console
- ordinary browser user-agent
- single threaded
- has ratelimit
- stable from connection, parsing errors


##Not implemented:

- proxy using
- captchas parsing
- robots constraints
- triggering additional generated html by js
- sessions using
- threading


Finish implementation depends on target pages research

## Quickstart

Example of script launch on Linux, Python 3.5:

```
$ git clone https://valram@bitbucket.org/valram/simple_crawler_email_extractor.git
$ cd simple_crawler_email_extractor
$ virtualenv .
$ source bin/activate
$ pip install -r requirements.txt  # alternatively try pip3
$ export info=1  # for logging process info messages to console
$ python crawler.py --depth 1 --url https://auto.mail.ru/forum/kurilka/

....
....
....

fetching https://r.mail.ru/n165232866?sz=11&rnd=135080557
status code : 200
Fetched 136 emails
....
....
....

Total 361 emails from https://auto.mail.ru/forum/kurilka/ with crawling_depth = 1

1  | irusik4u@mail.ru
2  | zolot-ka@mail.ru
3  | tanekudr@mail.ru
4  | bagavdin@mail.ru
5  | sever-rodina@mail.ru
6  | fedorko_80@mail.ru
7  | sweet-cherry19@mail.ru
8  | krauze_58@mail.ru
9  | sergovv1985@mail.ru
10 | sano7676@mail.ru
....
....
....

```


The code is written for educational purposes.
